# osim2urdf 

This library is used to convert .osim files into .urdf files. It is in part based on the ospi package.
For now it was tested on Ubuntu 18.04, Python 2.7 only, but it should also be working with Python 3.6. 
It was not tested with conda : all packages were directly installed into usr, without using a virtual environnement.

## Installation process

The installation process is a little bit complicated, but here's the main steps.

### Python packages

- numpy-stl : 

~~~
   pip install numpy-stl
~~~

### VMTK

Vmtk will be useful to convert .vtp files into .stl files, a format readable for .urdf.

Installation instructions could be found here : http://www.vmtk.org/download/. 

### Pinnochio 

For now, the majority of the package is based on the use of the Pinnochio package to perform matrix transformations.

It can be downloaded from here :  https://stack-of-tasks.github.io/pinocchio/download.html

### ROS Workspace

The written .urdf file is intended to be used within a ROS package. This was necessary to make sure that the .stl files could be read 
by the ROS packages used, and to easily set up solutions to control the .osim model joints.

To do so, do the following command into your ROS workspace : 

~~~
    catkin_create_pkg human_control controller_manager joint_state_controller robot_state_publisher
~~~

With human_control an example of the name of the ROS package used by the library osim2urdf.

Into the new human_control package, add a folder called rviz, and copy into this folder the file human_control.rviz (saved in the folder utilis_files). This Rviz
configuration will then be used by the package osim2urdf.

If you want to change the type of rviz files to be used or set a different name to the human_control package, please modify the begin_launchfile.txt file 
(saved in the folder utilis_files), and set the rviz node at your convenience.

## Use

Go to osim2urdf_parser.py. 
First, set up the variables ros_package_path, vtp_path, osim_path, filename, exactly_collision and exactly_visual of the file. The meaning of those variables is described in the documentation
of the constructor of the class Osim2URDF.
Execute the python file. 
Go to the ROS package containing the new .urdf file (ex : here, human_control). You can directly display and interact with the .urdf file with the command :

~~~
   roslaunch human_control human_control.launch
~~~

